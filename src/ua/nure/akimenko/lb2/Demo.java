package ua.nure.akimenko.lb2;

import java.sql.*;
import java.util.List;
import java.util.Map;

public class Demo {
	public static void main(String[] args) throws SQLException {
		var tchDao = new TeacherDAOMySQL();
		var tchServ = new TeacherService(tchDao);

		String[] tchNames = new String[] {
			"Иващенко Г. С.", "Мартовицкий В. О.", 
			"Литвинова Е. И.", "Росинский Д. Н."
		};
		Teacher[] tchArr = new Teacher[tchNames.length];
		for (int i = 0; i < tchNames.length; ++i) {
			var tch = new Teacher(tchNames[i]);
			tchServ.insertTeacher(tch);
			tchArr[i] = tch;
		}
		System.out.println("В БД добавлены такие учителя:");
		for (Teacher tch : tchArr) 
			System.out.println(tch);
		
		var tch = tchArr[tchArr.length - 1];
		String newName = "Росинский Дмитрий Николаевич";
		System.out.print("Отредактируем ФИО преподавателя " + tch.getFullName());
		System.out.println(" c заменением на " + newName);
		tch.setFullName(newName);
		tchServ.update(tch);
		System.out.println("Выполнено!\n\n");
		
		var sbjDao = new SubjectDAOMySql();
		var sbjServ = new SubjectService(sbjDao);
		
		String[] sbjTitles = new String[] {
			"Интернет технологии", "ОБД", "Python", "Oracle DB",
			"КЛог", "SoC", "Архитектура компьютеров"
		};
		Subject[] sbjArr = new Subject[sbjTitles.length];
		System.out.println("В БД добавлены такие предметы:");
		for (int i = 0; i < sbjTitles.length; ++i) {
			var sbj = new Subject(sbjTitles[i]);
			sbjServ.insert(sbj);
			System.out.println(sbj);
			sbjArr[i] = sbj;
		}
		
		Subject sbj = sbjArr[sbjArr.length - 1];
		String newTitle = "Архит. комп.";
		System.out.print("Отредактируем название предмета " + sbj.getTitle());
		System.out.println(" c заменением на " + newTitle);
		sbj.setTitle(newTitle);
		sbjServ.update(sbj);
		System.out.println("Выполнено!\n\n");
		
		var tchSbjDao = new TchSbjDAOMySQL();
		var tchSbjServ = new TchSbjService(tchSbjDao);
		
		Teacher martovitskiy = tchArr[1];
		int martovitskiyId = martovitskiy.getId(); 
		tchSbjServ.insert(tchArr[0].getId(), sbjArr[0].getId())
			.insert(martovitskiyId, sbjArr[1].getId())
			.insert(martovitskiyId, sbjArr[2].getId())
			.insert(martovitskiyId, sbjArr[3].getId())
			.insert(tchArr[2].getId(), sbjArr[4].getId())
			.insert(tchArr[2].getId(), sbjArr[5].getId())
			.insert(tchArr[3].getId(), sbjArr[6].getId());
		
		System.out.println("Таблица tch_sbj была заполнена следующими значениями:");
		List<Map.Entry<Integer, Integer>> list = tchSbjServ.selectAllPairs();
		for (var pair : list) {
			System.out.println(pair.getKey() + "\t" + pair.getValue());
		}
		

		System.out.println("\n\nВыведем предметы, которые ведет преподаватель " 
							+ martovitskiy.getFullName());
		System.out.println("Он ведет такие предметы:");
		DBManager db = DBManager.getInstance();
		List<Subject> sbjList = db.getSubjectsForTchById(martovitskiyId);
		for (Subject subj : sbjList) {
			System.out.println(subj.getTitle());
		}
	}
}
