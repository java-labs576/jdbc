package ua.nure.akimenko.lb2;

import java.sql.*;

public class SubjectDAOMySql {
	
	private static final String SQL_INSERT 
		= "INSERT INTO subjects(title) VALUES(?);";
	private static final String SQL_UPDATE
		= "UPDATE subjects SET title=? WHERE id=?;";
	
	public void insert(Connection conn, Subject sbj) throws SQLException  {
		PreparedStatement prepStm = conn.prepareStatement(SQL_INSERT, 
										PreparedStatement.RETURN_GENERATED_KEYS);
		prepStm.setString(1, sbj.getTitle());
		prepStm.executeUpdate();
		ResultSet res = prepStm.getGeneratedKeys();
		res.next();
		sbj.setId(res.getInt(1));
		prepStm.close();
	}
	
	public void update(Connection conn, Subject sbj) throws SQLException {
		PreparedStatement prepStm = conn.prepareStatement(SQL_UPDATE);
		prepStm.setString(1, sbj.getTitle());
		prepStm.setInt(2, sbj.getId());
		prepStm.executeUpdate();
		prepStm.close();
	}
}
