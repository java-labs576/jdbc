package ua.nure.akimenko.lb2;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DBManager {

	private static DBManager instance;
	
	private DBManager() {};
	
	public static synchronized DBManager getInstance() {
		if (instance == null) {
			instance = new DBManager();
		}
		return instance;
	}
	
	private static final String SQL_SELECT
		= "SELECT sbj.* FROM `teachers` tch"
			+ "	INNER JOIN tch_sbj ON tch.id=tch_sbj.tch_id"
			+ "    INNER JOIN subjects sbj ON sbj.id=tch_sbj.sbj_id"
			+ "    WHERE tch.id=?;";
	
	// returns list of some teacher's subjects by teacher id
	public List<Subject> getSubjectsForTchById(int tchId) throws SQLException {
		Connection conn = ConnectionFactory.getConnection();
		List<Subject> list = new ArrayList<>();
		PreparedStatement prepStm = conn.prepareStatement(SQL_SELECT);
		prepStm.setInt(1, tchId);
		ResultSet res = prepStm.executeQuery();
		while (res.next()) {
			list.add(
					new Subject(res.getInt(1), res.getString(2))
					);
		}
		res.close();
		prepStm.close();
		conn.commit();
		return list;
	}

}
