package ua.nure.akimenko.lb2;

import java.sql.*;

public class TeacherDAOMySQL {
	private static final String SQL_INSERT = 
			"INSERT INTO teachers (full_name) VALUES (?)";
	private static final String SQL_UPDATE = 
			"UPDATE teachers SET full_name=? WHERE id=?;";
	
	public void insert(Connection conn, Teacher tch) throws SQLException {
		PreparedStatement prepStm = conn.prepareStatement(SQL_INSERT,
										PreparedStatement.RETURN_GENERATED_KEYS);
		prepStm.setString(1, tch.getFullName());
		prepStm.executeUpdate();
		ResultSet res = prepStm.getGeneratedKeys();
		res.next();
		tch.setId(res.getInt(1));
		prepStm.close();
	}
	
	public void update(Connection conn, Teacher tch) throws SQLException {
		PreparedStatement prepStm = conn.prepareStatement(SQL_UPDATE);
		prepStm.setString(1, tch.getFullName());
		prepStm.setInt(2, tch.getId());
		prepStm.executeUpdate();
		prepStm.close();
	}
}
