package ua.nure.akimenko.lb2;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public class TchSbjService {
	private TchSbjDAOMySQL tchSbjDao;

	public TchSbjService(TchSbjDAOMySQL tchSbjDao) {
		this.tchSbjDao = tchSbjDao;
	}
	
	public TchSbjService insert(int tchId, int sbjId) throws SQLException {
		Connection conn = ConnectionFactory.getConnection();
		try {
			tchSbjDao.insert(conn, tchId, sbjId);
			conn.commit();
		} catch (Exception e) {
			conn.rollback();
			throw e;
		}
		return this;
	}

	public List<Map.Entry<Integer, Integer>> selectAllPairs() throws SQLException {
		Connection conn = ConnectionFactory.getConnection();
		List<Map.Entry<Integer, Integer>> list = null;
		try {
			list = tchSbjDao.selectAllPairs(conn);
			conn.commit();
		} catch (Exception e) {
			conn.rollback();
			throw e;
		}
		return list;
	}

}
