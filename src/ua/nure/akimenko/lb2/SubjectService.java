package ua.nure.akimenko.lb2;

import java.sql.*;

public class SubjectService {
	
	SubjectDAOMySql sbjDao;
	
	public SubjectService(SubjectDAOMySql sbjDao) {
		this.sbjDao = sbjDao;
	}
	
	public void insert(Subject sbj) throws SQLException {
		Connection conn = ConnectionFactory.getConnection();
		try {
			sbjDao.insert(conn, sbj);
			conn.commit();
		} catch (Exception e) {
			conn.rollback();
			throw e;
		}
	}
	
	public void update(Subject sbj) throws SQLException {
		Connection conn = ConnectionFactory.getConnection();
		try {
			sbjDao.update(conn, sbj);
			conn.commit();
		} catch (Exception e) {
			conn.rollback();
			throw e;
		}
	}
}
