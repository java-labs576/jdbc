package ua.nure.akimenko.lb2;

import java.sql.*;
import java.util.*;
import java.util.AbstractMap.SimpleEntry;

public class TchSbjDAOMySQL {
	private static final String SQL_INSERT 
		= "INSERT INTO tch_sbj(tch_id, sbj_id) VALUES(?, ?)";
	
	private static final String SQL_SELECT
		= "SELECT tch_id, sbj_id FROM `tch_sbj`;";
	
	public void insert(Connection conn, int tchId, int sbjId) throws SQLException {
		PreparedStatement prepStm = conn.prepareStatement(SQL_INSERT);
		prepStm.setInt(1, tchId);
		prepStm.setInt(2, sbjId);
		prepStm.executeUpdate();
		prepStm.close();
	}
	
	public List<Map.Entry<Integer, Integer>> selectAllPairs(Connection conn) throws SQLException {
		Statement stm = conn.createStatement();
		ResultSet res = stm.executeQuery(SQL_SELECT);
		List<Map.Entry<Integer, Integer>> list = new ArrayList<Map.Entry<Integer, Integer>>(); 
		while (res.next()) {
			int tchId = res.getInt(1);
			int sbjId = res.getInt(2);
			Map.Entry<Integer, Integer> pair = new SimpleEntry<Integer, Integer>(tchId, sbjId);
			list.add(pair);
		}
		return list;
	}
}
