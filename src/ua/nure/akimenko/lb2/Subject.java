package ua.nure.akimenko.lb2;

public class Subject {
	private int id;
	private String title;
	
	public Subject(String title) {
		this.title = title;
	}
	
	public Subject(int id, String title) {
		this.id = id;
		this.title = title;
	}	
	
	public int getId() {
		return id;
	}

	public Subject setId(int id) {
		this.id = id;
		return this;
	}
	
	public Subject setTitle(String title) {
		this.title = title;
		return this;
	}

	public String getTitle() {
		return title;
	}
	
	@Override
	public String toString() {
		return "[id: " + id + ", title: " + title + "]";
	}
}
