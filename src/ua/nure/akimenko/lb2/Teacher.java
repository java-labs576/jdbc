package ua.nure.akimenko.lb2;

public class Teacher {
	private int id;
	private String fullName;
	
	public Teacher setId(int id) {
		this.id = id;
		return this;
	}
	
	public int getId() {
		return id;
	}
	
	public Teacher setFullName(String fullName) {
		this.fullName = fullName;
		return this;
	}
	
	public String getFullName() {
		return fullName;
	}
	public Teacher(String fullName) {
		this.fullName = fullName;
	}

	@Override
	public String toString() {
		return "[id: " + id + ", fullName: " + fullName + "]";
	}	
}
