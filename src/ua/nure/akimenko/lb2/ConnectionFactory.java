package ua.nure.akimenko.lb2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {
	private static final String dbHost = "localhost:3306";
	private static final String dbName = "javaLb2Var11";
	private static final String dbUser = "root";
	private static final String dbPassword = "";

	private static String dbUrl = "jdbc:mysql://" + dbHost  + "/" + dbName ;

	public static Connection getConnection() throws SQLException {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (Exception e) {
			e.printStackTrace();
			throw new SQLException("Can't get mysql driver");
		}
		Connection conn = DriverManager.getConnection(dbUrl, dbUser, dbPassword);
		conn.setAutoCommit(false);
		
		return conn;
	}

}
