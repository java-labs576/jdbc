package ua.nure.akimenko.lb2;

import java.sql.Connection;
import java.sql.SQLException;

public class TeacherService {
	private TeacherDAOMySQL tchDAO;

	public TeacherService(TeacherDAOMySQL tchDAO) {
		this.tchDAO = tchDAO;
	}
	
	public void insertTeacher(Teacher tch) throws SQLException {
		Connection conn = ConnectionFactory.getConnection();
		try {
			tchDAO.insert(conn, tch);
			conn.commit();
		} catch (SQLException e) {
			conn.rollback();
			throw e;
		}
	}

	public void update(Teacher tch) throws SQLException {
		Connection conn = ConnectionFactory.getConnection();
		try {
			tchDAO.update(conn, tch);
			conn.commit();
		} catch (SQLException e) {
			conn.rollback();
			throw e;
		}
	}
}
